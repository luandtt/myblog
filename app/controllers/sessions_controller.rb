class SessionsController < ApplicationController
  def create
    @user = User.find_by("LOWER(username) = ?", user_params[:username].downcase)
    if @user.present?
      if @user.password == user_params[:password].downcase
        session[:current_name] = @user.last_name
        session[:current_level] = @user.level
        flash[:notice] = "login successfully"
        redirect_to :controller => 'posts', :action => 'index'
      else
        flash[:notice] = "wrong password"
        redirect_to :back
      end
    else
      flash[:notice] = "username not found"
      redirect_to :back
    end
  end

  def destroy
    session.clear
    redirect_to :back
  end
  
  private
    def user_params
      params.require(:user).permit(:username, :password)
    end
end