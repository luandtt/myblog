Rails.application.routes.draw do
  resources :users
  resources :posts do
    resources :comments
  end
  root 'posts#index'
  get 'about' => 'pages#about', as: :about
  get 'contact' => 'pages#contact', as: :contact
  get 'admin/posts' => 'managerposts#index'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  get 'logout' => 'sessions#destroy'
end
