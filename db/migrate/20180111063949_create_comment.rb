class CreateComment < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.string :commentname
      t.text :body
      t.references :posts, foreign_key: true

      t.timestamps
    end
  end
end
