class Post < ApplicationRecord
  has_many :comments, dependent: :destroy
  validates :title, presence: true

  extend FriendlyId
  friendly_id :title, use: :slugged

  def should_generate_friendly_id?
    title_changed?
  end
end