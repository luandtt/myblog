class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def create
    if params[:user][:repassword] == params[:user][:password]
      @user = User.new(user_params)
      if @user.save
        flash[:notice] = "Register successfully"
        redirect_to :controller => 'posts', :action => 'index'
      else
        redirect_to :back
      end
    else
      flash[:notice] = "Confirm password wrong"
      redirect_to :back
    end
  end

  private
    def user_params
      params.require(:user).permit(:username, :password, :email, :gender, :last_name)
    end
end